//
// Created by german on 26.04.2022.
//

#include "solution2.h"
#include <iostream>
#include <string>
#include <map>

bool up_person(std::string &new_person, std::string &old_person){ //true - новое слово должно быть выше в очереди

    int len;

    len = (new_person.length() > old_person.length()) ? new_person.length() : old_person.length();

    for(int i = 0; i < len; i++){
        if(new_person[i] < old_person[i]) return true;
    }

    if(len < old_person.length()) return true;
    else if(len > old_person.length()) return false;

    return false;
}

void sol2(){
    //std::cout << " Задача в разработке" << std::endl;
    //return;
    std::string ans = "start";
    std::map<std::string, int > RegList;

    while(1){

        std::cout << "Введите фамилию, Next (вызов следующего человека) или stop (выход)" << std::endl;
        std::cin >> ans;
        if(ans == "stop") break;
        else if(ans == "Next" ){
            if(RegList.empty()){
                std::cout<< "Очередь пуста"<< std::endl;
                continue;
            }
            std::cout<< RegList.begin()->first << std::endl;
            if(RegList.begin()->second > 1) RegList[RegList.begin()->first] -= 1;
            else RegList.erase(RegList.begin());
            continue;
        }
        else if (RegList.find(ans) == RegList.end()) {
            RegList.insert(std::pair<std::string, int>(ans, 1));
            continue;
        }
        RegList[ans] += 1;
    }
}

