#include <iostream>
#include "solution1.h"
#include "solution2.h"
#include "solution3.h"

using namespace std;

int main(){
    string ans;

    setlocale(0, "");
    while(ans != "stop"){
        cout << "Введите номер задачи для проверки(1, 2, 3) или \"stop\" для выхода" << endl;
        cin >> ans;
        switch (ans[0]) {
            case '1':
                sol1();
                break;
            case '2':
                sol2();
                break;
            case '3':
                sol3();
                break;
            default:
                break;
        }
    }
    return 0;
}


