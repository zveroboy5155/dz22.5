#include <iostream>

#include "solution3.h"
#include <cmath>
#include <string>
#include <map>

bool check_strs(std::string &str1, std::string &str2)
{
    std::map<int,int> word_1;
    bool res;
    char cur_ch;
    std::map<int,int>::iterator it;

    if(str1.length() != str2.length()) return false;
    for(int i = 0; i < str1.length(); i++){
        if(word_1.find(str1[i]) == word_1.end()) word_1.insert(std::pair<int,int>(str1[i], 1));
        else word_1[str1[i]] += 1;
    }


    for(int i = 0; i < str2.length(); i++){
        cur_ch = str2[i];
        it = word_1.find(cur_ch);
        if ( it != word_1.end()) {
            if(it->second > 1)
                word_1[cur_ch] -= 1;
            else
                (word_1.erase(it));
        }else return false;
    }
    return true;
}
void sol3(){
    //std::cout << " Задача в разработке" << std::endl;
    std::string str1, str2;

    std::cout << "Введите 2 слова для сравнения" << std::endl;
    std::cin >> str1 >> str2;
    std::cout << (check_strs(str1, str2) ? "Слова являются" : "Слова не являются") << " анаграммами" << std::endl;

}