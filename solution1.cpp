#include "./solution1.h"
#include <iostream>
#include <string>
#include <map>
#include <vector>

bool is_num(char &c){
    return (c >= '0' && c <= '9');
}

void sol1(){
    std::string ans = "start";
    std::map<std::string, std::string> dictPhone;
    std::map<std::string, std::vector<std::string> > SurnameToNumber;
    std::string surname, number;
    std::vector<std::string> tmp_vec;

    std::cin.ignore();
    while(1){
        std::cout << "Введите запрос, для выхода введите stop" << std::endl;
        getline(std::cin,ans);
        if (ans == "stop") break;
        else if (is_num(ans[0])){
            tmp_vec.clear();
            if(!is_num(ans[ans.length() - 1])) {
                surname = ans.substr(ans.find(' '));
                number = ans.substr(0, ans.find(' '));
                dictPhone.insert(std::make_pair<std::string, std::string>(number, surname));
                if(SurnameToNumber.find(surname) == SurnameToNumber.end())
                    SurnameToNumber.insert(std::pair<std::string, std::vector<std::string> >(surname, tmp_vec));
                SurnameToNumber[surname].push_back(number);
            }
            else std::cout << dictPhone[ans] << std::endl;
        }else{
            std::cout << "Ищем номер/номера для " << ans << std::endl;
            if (SurnameToNumber.find(surname) == SurnameToNumber.end() ) {
                std::cout << "Неизвестный абонент" << ans << std::endl;
                continue;
            }
            tmp_vec = SurnameToNumber[surname];
            for(int i = 0; i < tmp_vec.size(); i++) std::cout << tmp_vec[i]  << std::endl;
        }
    }

}